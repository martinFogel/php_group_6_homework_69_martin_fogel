<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * @group access_to_all_comments
     * @return void
     */
    public function test_success_get_all_comments()
    {
        $user = \App\Models\User::factory()->create();
        Passport::actingAs($user, ['*']);
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comments = \App\Models\Comment::factory()->for($user)->create();
        $response = $this->getJson(route('comments.index'));
        $response->assertOk();
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'total',
                    'to',
                    'per_page'
                ],
                'links'
            ]
        );
        $this->assertCount($comments->count(), $response->json()['data']);
    }

    /**
     * @group access_to_all_comments
     * @return void
     */
    public function test_not_auth_user_trying_get_all_comments()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        \App\Models\Comment::factory()->for($user)->count(3)->create();
        $response = $this->getJson(route('comments.index'));
        $response->assertUnauthorized();
    }

    /**
     * @group @group access_to_one_comment
     * @return void
     */
    public function test_user_success_get_once_comment()
    {
        $user = \App\Models\User::factory()->create();
        \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        Passport::actingAs($user, ['*']);
        $response = $this->getJson(route('comments.show', compact('comment')));
        $response->assertOk();
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'body',
                    'user' => [
                        'id',
                        'email'
                    ],
                    'article' => [
                        'id',
                    ]
                ]
            ]
        );
        $this->assertEquals($comment->id, $response->json()['data']['id']);
        $this->assertEquals($comment->body, $response->json()['data']['body']);
        $this->assertEquals($comment->user_id, $response->json()['data']['user']['id']);
    }

    /**
     * @group @group access_to_one_comment
     * @return void
     */
    public function test_not_auth_user_trying_get_once_comment()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $response = $this->getJson(route('comments.show', compact('comment')));
        $response->assertUnauthorized();
    }

    /**
     * @group delete_comment
     * @return void
     */
    public function test_user_success_delete_comment()
    {
        $user = \App\Models\User::factory()->create();
        Passport::actingAs($user, ['*']);
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
        $response = $this->deleteJson(route('comments.destroy', compact('comment')));
        $response->assertNoContent();
        $this->assertDatabaseMissing('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
    }

    /**
     * @group delete_comment
     * @return void
     */
    public function test_user_can_not_delete_not_his_comment()
    {
        $user = \App\Models\User::factory()->create();
        $another_user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        Passport::actingAs($another_user, [route('comments.destroy', compact('comment'))]);
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
        $response = $this->deleteJson(route('comments.destroy', compact('comment')));
        $response->assertForbidden();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
    }

    /**
     * @group delete_comment
     * @return void
     */
    public function test_not_auth_user_can_not_delete_comment()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
        $response = $this->deleteJson(route('comments.destroy', compact('comment')));
        $response->assertUnauthorized();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
    }

    /**
     * @group store_comment
     * @return void
     */
    public function test_not_auth_user_can_not_store_comment()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
        $data = [
            'body' => 'My awesome body',
        ];
        $response = $this->postJson(route('comments.store', ['article' => $article]), $data);
        $response->assertUnauthorized();
    }

    /**
     * @group store_comment
     * @return void
     */
    public function test_auth_user_can_store_comment()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
        Passport::actingAs($user, [route('comments.store', ['article' => $article])]);
        $data = [
            'body' => 'My awesome body',
        ];
        $response = $this->postJson(route('comments.store', ['article' => $article]), $data);
        $response->assertCreated()->json();
    }

    /**
     * @group update_comment
     * @return void
     */
    public function test_not_auth_user_can_not_update_comment()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $response = $this->patchJson(route('comments.update', compact('comment')))->assertUnauthorized();
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
    }

    /**
     * @group update_comment
     * @return void
     */
    public function test_auth_user_can_update_comment()
    {
        $user = \App\Models\User::factory()->create();
        Passport::actingAs($user, ['*']);
        $article = \App\Models\Article::factory()->for($user)->count(3)->create();
        $comment = \App\Models\Comment::factory()->for($user)->create();
        $response = $this->patchJson('comments.update', compact('comment'));
        $this->assertDatabaseHas('comments', [
            'body' => $comment->body,
            'user_id' => (string)$comment->user_id,
            'article_id' => (string)$comment->article_id,
            'id' => (string)$comment->id
        ]);
    }
}
