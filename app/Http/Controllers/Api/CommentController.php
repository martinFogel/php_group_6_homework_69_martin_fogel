<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\CommentStoreRequest;
use App\Http\Resources\CommentResource;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $comments = Comment::paginate(5);
        return CommentResource::collection($comments);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CommentStoreRequest $request
     * @param Article $article
     * @return JsonResponse
     */
    public function store(CommentStoreRequest $request, Article $article)
    {
        $data = $request->validated();
        $data['user_id'] = auth('api')->user()->getAuthIdentifier();
        $data['article_id'] = $article->id;
        $comment = Comment::create($data);
        return response()->json(new CommentResource($comment), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Comment $comment
     * @return CommentResource
     */
    public function show(Comment $comment)
    {
        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentStoreRequest $request
     * @param Comment $comment
     * @return CommentResource
     */
    public function update(CommentStoreRequest $request, Comment $comment)
    {
        $comment->update($request->validated());
        $comment->fresh();
        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return response([], 204);
    }
}
